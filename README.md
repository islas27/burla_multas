# BurlaMultas #

Aplicación android que muestra en un mapa de la ciudad las cámaras de fotomulta, así como emite una alarma sonora al acercarse al rango de una de ellas.

Utiliza como backend una API montada en heroku, desarrollada por otro developer para hacer el calculo de distancias (locación actual vs locación de las cámaras). Este se encuentra en el siguiente repositorio:

[GitHub](https://github.com/alexescg/hackuu)
